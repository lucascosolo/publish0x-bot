const curl = require('curl');
const jsdom = require('jsdom');

const he = require('he');
const emoji = require('node-emoji');

const url = 'https://www.publish0x.com/category/crypto?filter=latest&type=latest';

const Twit = require('twit');
const config = require('./config.json');

var bot = new Twit(config);

const website = 'https://www.publish0x.com';
const referral_id = '?a=nXe0rZVbxr&tid=curated';

var queue = {};
const already_tweeted = [];

function parseData(html) {
  const {JSDOM} = jsdom;
  const dom = new JSDOM(html);
  const $ = (require('jquery'))(dom.window);
  
  var posts = $('.blogname');
  for (var i = posts.length - 1; i >= 0 ; i--) {
    var post = $(posts[i]);
    var title = he.decode(post.html().trim());
    var url = post.attr('href');
    if (already_tweeted.indexOf(url) != -1) continue;
    if (already_tweeted.some(id => id === url)) continue;
    queue = {
      title,
      url
    };
    break;
  }
}

function loadData() {
  curl.get(url, null, (err,res,body)=>{
    if(res.statusCode == 200){
      parseData(body);
      tweet();
      setTimeout(loadData, (1000 * 60 * 60) + ((Math.random() - 0.5) * 1000 * 60 * 5));
    } else {
      console.error(err);
    }
  });
}

String.prototype.insert = function(index, string) {
  if (index > 0) {
    return this.slice(0, index) + string + this.slice(index, this.length);
  }
  return string + this;
};

const hashtagKeywords = ['bitcoin', 'steemit', 'vr', 'privacy', 'blockchain', 'litecoin', 'ethereum', 'chainlink', 'coinbase', 'eosio', 'crypto', 'hydro', 'investment', 'cryptocurrency', 'binance', 'monero', 'facebook', 'scam', 'scams', 'tezos'];
const tickerKeywords = ['tron', 'btc', 'ltc', 'doge', 'xlm', 'xmr', 'steem', 'eth', 'xtz', 'dgb', 'tzc'];

function hashtags(_str) {
  var str = ' ' + _str + ' ';
  var lower = str.toLowerCase();
  for (var i in hashtagKeywords) {
    var index = lower.indexOf(' ' + hashtagKeywords[i] + ' ');
    if (index == -1) continue;
    lower = lower.insert(index + 1, '#')
    str = str.insert(index + 1, '#');
  }
  for (var i in tickerKeywords) {
    var index = lower.indexOf(' ' + tickerKeywords[i] + ' ');
    if (index == -1) continue;
    lower = lower.insert(index + 1, '$');
    str = str.insert(index + 1, '$');
  }
  return str.substring(1, str.length - 1);
}

const quips = [
  'The article platform where users collect cryptocurrencies like $ETH and $LRC!',
  'Read articles, tip authors and get #FreeCrypto!',
  'Awesome $BAT and $ETH faucet',
  'When you write articles on #Publish0x, readers can tip you crypto and get some for themselves!',
  'Read this article and more like it on Publish0x',
  'Become a writer on #Publish0x',
  'Crypto-based blogging',
  'News and blog website with #crypto tips'
]

function quip() {
  return quips[Math.floor(Math.random() * quips.length)];
}
 
const emojis = [
  emoji.emojify(":hibiscus:"),
  emoji.emojify(":ribbon:"),
  emoji.emojify(":fire:"),
  emoji.emojify(":clipboard:"),
  emoji.emojify(':gem:'),
  emoji.emojify(':sparkles:'),
  emoji.emojify(':zap:'),
  emoji.emojify(':blossom:'),
  emoji.emojify(':tulip:'),
  emoji.emojify(':rainbow:'),
  emoji.emojify(':star:'),
  emoji.emojify(':heart:')
]

function getemoji() {
  return emojis[Math.floor(Math.random() * emojis.length)];
}

function tweet() {
  if (queue == null) {
    setTimeout(tweet, 1000 * 60 * 10);
  } else {
    var post = queue;
    var status = hashtags(post.title) + '\n\n' +
    getemoji() + ' ' + 'Curated post from Publish0x' + '\n' + 
    getemoji() + ' ' + quip() + ' #ad\n' +
     website + post.url + referral_id;
    bot.post('statuses/update', { status }, (err, data, res) => {
      if (!err) {
        console.log('- - - -\n' + status)
        already_tweeted.push(url);
        while (already_tweeted.length > 50) {
          already_tweeted.shift();
        }
        queue = null;
      }
    });
  }
}

var binance_quips = [
  'Low fees',
  'Options to earn more on your crypto',
  'Easy for new traders',
  'Simple sign up process'
]

function binance_quip() {
  return binance_quips[Math.floor(Math.random() * binance_quips.length)];
}

function binance() {
  var status = '#USA users can join BinanceUS to start investing in #Bitcoin and other cryptos.\n\n'
                + getemoji() + ' ' + binance_quip() + '\n'
                + 'Use my link to help me out:'
                + 'http://bit.ly/BuyCryptoUSA';
  bot.post('statuses/update', { status }, (err, data, res) => {
    if (!err) {
      console.log('- - - - \n' + status);
    }
    setTimeout(binance, 1000 * 60 * 60 * 6);
  })
}

loadData();
//binance();
